# Payungku : a Weather App 

Payungku is a weather app developed using Flutter and OpenWeatherMap API to show you weather in selected city. It will show you:
1. Current Weather
2. Detailed parameter current weather
3. Hourly forecast for 24 hours
4. Daily forecast for 5 days per 3 hours 

<!-- ![Img 1](screenshot/sc_1.jpg) ![Img 2](screenshot/sc_2.jpg) ![Img 3](screenshot/sc_3.jpg) -->
<p float="left">
  <img src="screenshot/sc_1.jpg" width="200" />
  <img src="screenshot/sc_2.jpg" width="200" /> 
  <img src="screenshot/sc_3.jpg" width="200" />
</p>

This app also using dropdown_searchable package for convenience in selecting city.

## Installation
to install this app in your android, just download 'app-release.apk' and install it.

##Video Demo
You can watch the demonstation of the app from the video in link below
https://youtu.be/RIe-jBcPWE8
