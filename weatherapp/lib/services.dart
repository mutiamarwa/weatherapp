import 'dart:convert';

import 'package:http/http.dart' as http;

import 'models/forcast.dart';
import 'models/forcast_fiveday.dart';
import 'models/location.dart';
import 'models/weather.dart';

Future getCurrentWeather(Location location) async {
  Weather? weather;
  String city = location.city;
  String lat = location.lat;
  String lon = location.lon;
  String apiKey = "1b5769fc5f0cd70605eceac461224cc8";
  var url =
      "https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&appid=$apiKey&units=metric";

  final response = await http.get(Uri.parse(url));

  if (response.statusCode == 200) {
    weather = Weather.fromJson(jsonDecode(response.body));
  }

  return weather;
}

Future getForecast(Location location) async {
  Forecast? forecast;
  String apiKey = "1b5769fc5f0cd70605eceac461224cc8";
  String lat = location.lat;
  String lon = location.lon;
  String city = location.city;
  var url =
      "https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$lon&appid=$apiKey&units=metric";

  var urlx =
      "https://api.openweathermap.org/data/2.5/onecall?q=$city&appid=$apiKey&units=metric";

  final response = await http.get(Uri.parse(url));

  if (response.statusCode == 200) {
    forecast = Forecast.fromJson(jsonDecode(response.body));
  }

  return forecast;
}

Future getForecastFiveDays(Location location) async {
  ForecastFiveDays? forecast5days;
  String apiKey = "1b5769fc5f0cd70605eceac461224cc8";
  String lat = location.lat;
  String lon = location.lon;
  String city = location.city;
  var url =
      "https://api.openweathermap.org/data/2.5/forecast?lat=$lat&lon=$lon&appid=$apiKey&units=metric";

  final response = await http.get(Uri.parse(url));

  if (response.statusCode == 200) {
    forecast5days = ForecastFiveDays.fromJson(jsonDecode(response.body));
  }

  return forecast5days;
}
