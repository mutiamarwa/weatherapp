import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserController extends GetxController {
  static UserController instance = Get.put(UserController());
  TextEditingController username = TextEditingController();
  Rx<String> user = ''.obs;

  void setUsername() {
    user.value = username.text;
  }

  String getUsername() {
    return user.value;
  }
}
