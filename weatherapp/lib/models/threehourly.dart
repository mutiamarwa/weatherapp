class ThreeHourly {
  final int dt;
  final double temp;
  final double feelsLike;
  final double pressure;
  // final double dewPoint;
  // final double uvi;
  final double visibility;
  final double wind;
  final String description;
  final String icon;

  ThreeHourly(
      {required this.dt,
      required this.temp,
      required this.feelsLike,
      required this.pressure,
      // required this.dewPoint,
      // required this.uvi,
      required this.visibility,
      required this.wind,
      required this.description,
      required this.icon});

  factory ThreeHourly.fromJson(Map<String, dynamic> json) {
    return ThreeHourly(
      dt: json['dt'].toInt(),
      temp: json['main']['temp'].toDouble(),
      feelsLike: json['main']['feels_like'].toDouble(),
      pressure: json['main']['pressure'].toDouble(),
      // dewPoint: json['dew_point'].toDouble(),
      // uvi: json['uvi'].toDouble(),
      visibility: json['visibility'].toDouble(),
      wind: json['wind']['speed'].toDouble(),
      description: json['weather'][0]['description'],
      icon: json['weather'][0]['icon'],
    );
  }
}
