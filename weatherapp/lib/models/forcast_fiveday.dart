import 'package:intl/intl.dart';

import '../utils.dart';
import 'daily.dart';
import 'threehourly.dart';
// import 'hourly.dart';

class ForecastFiveDays {
  final List<List<ThreeHourly>> threehourly;
  // final List<Daily> daily;

  ForecastFiveDays({required this.threehourly});

  factory ForecastFiveDays.fromJson(Map<String, dynamic> json) {
    List<dynamic> threeHourlyData = json['list'];
    List<List<ThreeHourly>> threehourlydaily = [];
    List<ThreeHourly> threehourly = [];
    // int index = 0;
    DateTime date = DateTime.now();
    String datetoday = DateFormat('dd/MM').format(date);
    String datetemp = DateFormat('dd/MM').format(date);

    threeHourlyData.forEach((item) {
      var hour = ThreeHourly.fromJson(item);
      String datefromhour = getDateFromTimestamp(hour.dt);
      if (datetoday != datefromhour) {
        if (datetemp == datefromhour) {
          threehourly.add(hour);
        } else {
          datetemp = datefromhour;
          if (threehourly.isNotEmpty) {
            threehourlydaily.add(threehourly);
          }
          threehourly = [];
          threehourly.add(hour);
        }
      }
    });
    threehourlydaily.add(threehourly);

    return ForecastFiveDays(threehourly: threehourlydaily);
  }
}

mixin threeHourly {}
