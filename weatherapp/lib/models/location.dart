import 'dart:convert';

List<Location> locationFromJson(String str) =>
    List<Location>.from(json.decode(str).map((x) => Location.fromJson(x)));

class Location {
  final String city;
  // final String state;
  final String country;
  final String lat;
  final String lon;

  Location(
      {required this.city,
      // required this.state,
      required this.country,
      required this.lat,
      required this.lon});

  factory Location.fromJson(Map<String, dynamic> json) {
    // print(json);
    return Location(
      city: json['name'],
      // state: json['state'],
      country: json['country'],
      lat: json['lat'].toString(),
      lon: json['lon'].toString(),
    );
  }
}
