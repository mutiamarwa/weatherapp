import 'dart:io';

import 'package:dropdown_search/dropdown_search.dart';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weatherapp/currentWeather.dart';
import 'package:weatherapp/models/location.dart';
import 'constants/alamat_indo.dart';
import 'controller/controllers.dart';

class FormPage extends StatefulWidget {
  const FormPage({Key? key}) : super(key: key);

  @override
  State<FormPage> createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  Map<String, dynamic>? _myProvinsi;
  Map<String, dynamic>? _myKota;
  String? name;
  // Map<String, dynamic>? _myKecamatan;
  List<Map<String, dynamic>>? _kotafilter;
  late List<Location> locations;
  late Location location;

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    // var _kecamatanfilter = kecamatans
    //     .where((kecamatan) => kecamatan["regency_id"] == "$_myKota")
    //     .toList();

    return Scaffold(
      // resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Text("Payungku",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w600,
                        color: Colors.indigo)),
                // SizedBox(
                //   height: 10,
                // ),
                // Text("Siap dengan segala cuaca",
                //     textAlign: TextAlign.center,
                //     style: TextStyle(
                //         fontSize: 12,
                //         fontWeight: FontWeight.w400,
                //         color: Colors.indigo[300])),
                // SizedBox(
                //   height: 8,
                // ),
                Padding(
                  padding: const EdgeInsets.all(8),
                  child: Image.asset(
                    "assets/images/season-change.png",
                    height: 200,
                    width: 200,
                  ),
                ),
                Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Container(
                          height: 70,
                          padding: EdgeInsets.all(10),
                          child: TextFormField(
                            controller: userController.username,
                            validator: validateUsername,
                            keyboardType: TextInputType.name,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Username",
                              labelStyle: TextStyle(
                                  fontSize: 14, color: Colors.grey[700]),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Container(
                          height: 70,
                          padding: EdgeInsets.all(10),
                          child: DropdownSearch<Map<String, dynamic>>(
                            mode: Mode.MENU,
                            items: provinsis,
                            onChanged: (value) {
                              setState(() {
                                _myKota = null;
                                // _myKecamatan = null;
                                _myProvinsi = value;
                                if (value != null) {
                                  _kotafilter = kotas
                                      .where((kota) =>
                                          kota["province_id"] ==
                                          "${_myProvinsi!['id']}")
                                      .toList();
                                }
                              });
                            },
                            selectedItem: _myProvinsi,
                            showClearButton: true,
                            showSearchBox: true,
                            popupItemBuilder: (context, item, isSelected) =>
                                ListTile(
                                    textColor: Colors.grey[700],
                                    title: Text(item['name'].toString())),
                            dropdownBuilder: (context, selectedItem) => Text(
                              selectedItem?["name"].toString() ??
                                  "Pilih Provinsi",
                              style: TextStyle(color: Colors.grey[700]),
                            ),
                            validator: (x) {
                              if (!provinsis.contains(x) || x!.isEmpty) {
                                return 'Provinsi belum dipilih';
                              }
                              return null;
                            },
                          ),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Container(
                          height: 70,
                          padding: EdgeInsets.all(10),
                          child: DropdownSearch<Map<String, dynamic>>(
                            mode: Mode.MENU,
                            items: _kotafilter,
                            onChanged: (value) {
                              _myKota = value;
                            },
                            selectedItem: _myKota,
                            showClearButton: true,
                            showSearchBox: true,
                            popupItemBuilder: (context, item, isSelected) =>
                                ListTile(
                                    textColor: Colors.grey[700],
                                    title: Text(
                                        "${item['type']} ${item['name']}")),
                            dropdownBuilder: (context, selectedItem) => Text(
                                (selectedItem?["name"].toString() != null)
                                    ? "${selectedItem?['type']} ${selectedItem?['name']}"
                                    : "Pilih Kota",
                                style: TextStyle(color: Colors.grey[700])),
                            validator: (x) {
                              if (!kotas.contains(x) || x!.isEmpty) {
                                return 'Kota belum dipilih';
                              }
                              return null;
                            },
                          ),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Container(
                          height: 50,
                          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: ElevatedButton(
                              style: raisedButtonStyle_dark,
                              child: Text("Cek Cuaca"),
                              onPressed: () async {
                                if (_formKey.currentState!.validate()) {
                                  var city = _myKota!['name'];
                                  try {
                                    String apiKey =
                                        "1b5769fc5f0cd70605eceac461224cc8";
                                    var url =
                                        "http://api.openweathermap.org/geo/1.0/direct?q=$city,ID&appid=$apiKey";
                                    final response =
                                        await http.get(Uri.parse(url));
                                    if (response.statusCode == 200) {
                                      locations =
                                          locationFromJson(response.body);
                                      location = locations[0];
                                    }
                                    userController.setUsername();
                                    Get.to(
                                        CurrentWeatherPage(location: location));
                                  } catch (e) {
                                    if (e is SocketException) {
                                      Get.snackbar(
                                        "get location",
                                        "location message",
                                        backgroundColor: Colors.grey,
                                        snackPosition: SnackPosition.BOTTOM,
                                        titleText: Text(
                                          "Please check your internet and connection..",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        messageText: Text(
                                          e.message,
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      );
                                    } else {
                                      Get.snackbar(
                                        "Failed to get location",
                                        "location message",
                                        backgroundColor: Colors.grey,
                                        snackPosition: SnackPosition.BOTTOM,
                                        // titleText: Text(
                                        //   "Location not found",
                                        //   style: TextStyle(color: Colors.white),
                                        // ),
                                        messageText: Text(
                                          e.toString(),
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      );
                                    }
                                    // ScaffoldMessenger.of(context).showSnackBar(
                                    //     SnackBar(content: Text(e.toString())));
                                  }
                                }
                              }),
                        ),
                        // SizedBox(
                        //   height: 50,
                        // )
                      ],
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}

String? validateUsername(String? formUsername) {
  if (formUsername == null || formUsername.isEmpty)
    return 'Harap masukan Username';

  String pattern = r'^(?=.*[a-z]).{3,}$';
  RegExp regex = RegExp(pattern);
  if (!regex.hasMatch(formUsername)) return '''Min. 3 karakter''';

  return null;
}

final ButtonStyle raisedButtonStyle_dark = ElevatedButton.styleFrom(
  onPrimary: Colors.grey[300],
  primary: Colors.indigoAccent,
  //minimumSize: Size(88, 36),
  minimumSize: const Size.fromHeight(50),
  padding: EdgeInsets.symmetric(horizontal: 16),

  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);

// Future getLocation(String kota) async {
//   Location? location;
//   String city = kota;
//   String apiKey = "1b5769fc5f0cd70605eceac461224cc8";
//   var url =
//       "http://api.openweathermap.org/geo/1.0/direct?q=$city,id&appid=$apiKey";

//   final response = await http.get(Uri.parse(url));

//   try {
//     if (response.statusCode == 200) {
//       location = Location.fromJson(jsonDecode(response.body));
//     }
//   } catch (e) {
//     Get.snackbar(
//       "get location",
//       "location message",
//       backgroundColor: Colors.orangeAccent,
//       snackPosition: SnackPosition.BOTTOM,
//       titleText: Text(
//         "Location not found",
//         style: TextStyle(color: Colors.white),
//       ),
//       messageText: Text(
//         e.toString(),
//         style: TextStyle(
//           color: Colors.white,
//         ),
//       ),
//     );
//   }

//   return location;
// }
