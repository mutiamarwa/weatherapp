import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import 'dart:convert';
import 'controller/controllers.dart';
import 'extensions.dart';
import 'package:intl/intl.dart';

import 'models/forcast.dart';
import 'models/forcast_fiveday.dart';
import 'models/location.dart';
import 'models/weather.dart';
import 'services.dart';
import 'utils.dart';

class CurrentWeatherPage extends StatefulWidget {
  late final Location location;
  CurrentWeatherPage({Key? key, required this.location}) : super(key: key);

  @override
  _CurrentWeatherPageState createState() => _CurrentWeatherPageState(location);
}

class _CurrentWeatherPageState extends State<CurrentWeatherPage> {
  late final Location location;
  _CurrentWeatherPageState(this.location);
  var name = userController.getUsername();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[100],
        body: ListView(children: <Widget>[
          currentWeatherViews(location, name),
          forcastViewsHourly(location),
          forcastViewsThreeHourly(location),
          // forcastViewsDaily(this.location),
        ]));
  }
}

Widget currentWeatherViews(Location location, String name) {
  Weather _weather;

  return FutureBuilder(
    builder: (context, snapshot) {
      if (snapshot.hasData) {
        _weather = snapshot.data as Weather;
        if (_weather == null) {
          return Text("Error getting weather");
        } else {
          return Column(children: [
            createAppBar(location),
            weatherBox(_weather, name),
            weatherDetailsBox(_weather),
          ]);
        }
      } else {
        return Center(child: CircularProgressIndicator());
      }
    },
    future: getCurrentWeather(location),
  );
}

Widget forcastViewsHourly(Location location) {
  Forecast _forcast;

  return FutureBuilder(
    builder: (context, snapshot) {
      if (snapshot.hasData) {
        _forcast = snapshot.data as Forecast;
        if (_forcast == null) {
          return Text("Error getting weather");
        } else {
          return hourlyBoxes(_forcast);
        }
      } else {
        return Center(child: CircularProgressIndicator());
      }
    },
    future: getForecast(location),
  );
}

Widget forcastViewsThreeHourly(Location location) {
  ForecastFiveDays _forcast;

  return FutureBuilder(
    builder: (context, snapshot) {
      if (snapshot.hasData) {
        _forcast = snapshot.data as ForecastFiveDays;
        if (_forcast == null) {
          return Text("Error getting weather");
        } else {
          return threeHourlyBoxes(_forcast);
        }
      } else {
        return Center(child: CircularProgressIndicator());
      }
    },
    future: getForecastFiveDays(location),
  );
}

Widget createAppBar(Location location) {
  DateTime date = DateTime.now();
  String formatteddate = DateFormat('EEEE, dd MMM yyyy').format(date);
  return Container(
    padding: const EdgeInsets.only(left: 20, right: 20),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          // padding: const EdgeInsets.only(left: 20, right: 40),
          child: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Icon(Icons.arrow_back)),
        ),
        Container(
            padding:
                const EdgeInsets.only(left: 20, top: 5, bottom: 5, right: 20),
            margin: const EdgeInsets.only(
                top: 15, left: 15.0, bottom: 5.0, right: 15.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(60)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3),
                  )
                ]),
            child: Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                              text: '${location.city.capitalizeFirstOfEach}, ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16)),
                          TextSpan(
                              text: '${location.country.capitalizeFirstOfEach}',
                              // text: 'Indonesia',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal, fontSize: 16)),
                        ],
                      ),
                    ),
                    // Icon(
                    //   Icons.keyboard_arrow_down_rounded,
                    //   color: Colors.black,
                    //   size: 24.0,
                    //   semanticLabel: 'Tap to change location',
                    // ),
                  ],
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  formatteddate,
                  style: TextStyle(fontSize: 12, color: Colors.grey),
                )
              ],
            )),
        Container(
            // padding: const EdgeInsets.only(left: 20, right: 40),
            child: GestureDetector(
          onTap: () {},
          child: Icon(Icons.settings),
        )),
      ],
    ),
  );
}

Widget weatherDetailsBox(Weather _weather) {
  return Container(
    padding: const EdgeInsets.only(left: 15, top: 25, bottom: 25, right: 15),
    margin: const EdgeInsets.only(left: 15, top: 5, bottom: 15, right: 15),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          )
        ]),
    child: Row(
      children: [
        Expanded(
            child: Column(
          children: [
            Container(
                child: Text(
              "Wind",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 12,
                  color: Colors.grey),
            )),
            Container(
                child: Text(
              "${_weather.wind} km/h",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 12,
                  color: Colors.black),
            ))
          ],
        )),
        Expanded(
            child: Column(
          children: [
            Container(
                child: Text(
              "Humidity",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 12,
                  color: Colors.grey),
            )),
            Container(
                child: Text(
              "${_weather.humidity.toInt()}%",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 12,
                  color: Colors.black),
            ))
          ],
        )),
        Expanded(
            child: Column(
          children: [
            Container(
                child: Text(
              "Pressure",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 12,
                  color: Colors.grey),
            )),
            Container(
                child: Text(
              "${_weather.pressure.toInt()} hPa",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 12,
                  color: Colors.black),
            ))
          ],
        )),
        Expanded(
            child: Column(
          children: [
            Container(
                child: Text(
              "Cloud",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 12,
                  color: Colors.grey),
            )),
            Container(
                child: Text(
              "${_weather.cloud}%",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 12,
                  color: Colors.black),
            ))
          ],
        ))
      ],
    ),
  );
}

Widget weatherBox(Weather _weather, String name) {
  return Stack(children: [
    Container(
      padding: const EdgeInsets.all(15.0),
      margin: const EdgeInsets.all(15.0),
      height: 160.0,
      decoration: BoxDecoration(
          color: Colors.indigoAccent[200],
          borderRadius: BorderRadius.all(Radius.circular(20))),
    ),
    ClipPath(
        clipper: Clipper(),
        child: Container(
            padding: const EdgeInsets.all(15.0),
            margin: const EdgeInsets.all(15.0),
            height: 160.0,
            decoration: BoxDecoration(
                color: Colors.indigoAccent[400],
                borderRadius: BorderRadius.all(Radius.circular(20))))),
    Container(
        padding: const EdgeInsets.all(15.0),
        margin: const EdgeInsets.all(15.0),
        height: 160.0,
        decoration:
            BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Column(
          children: [
            Greetings(name),
            Row(
              children: [
                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                          child: Text(
                        "${_weather.temp.toInt()}°",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 60,
                            color: Colors.white),
                      )),
                      Container(
                          margin: const EdgeInsets.all(0),
                          child: Text(
                            "Feels like ${_weather.feelsLike.toInt()}°",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 13,
                                color: Colors.white),
                          )),
                    ]),
                Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                      getWeatherIcon(_weather.icon),
                      Container(
                          margin: const EdgeInsets.fromLTRB(5, 0, 5, 5),
                          child: Text(
                            "${_weather.description.capitalizeFirstOfEach}",
                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 16,
                                color: Colors.white),
                          )),
                      Container(
                          margin: const EdgeInsets.fromLTRB(5, 0, 5, 5),
                          child: Text(
                            "H:${_weather.high.toInt()}° L:${_weather.low.toInt()}°",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 13,
                                color: Colors.white),
                          )),
                    ])),
              ],
            ),
          ],
        ))
  ]);
}

Widget Greetings(String name) {
  var timeNow = DateTime.now().hour;
  return Container(
      // padding: EdgeInsets.only(left: 20),
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          if (timeNow <= 16) ...[
            Text(
              "${greetingMessage()}, ",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: Colors.amberAccent,
              ),
            ),
          ] else ...[
            Text(
              "${greetingMessage()}, ",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: Colors.indigo[1000],
              ),
            ),
          ],
          Text("${name} !",
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: Colors.white,
              ))
        ],
      ));
}

String greetingMessage() {
  var timeNow = DateTime.now().hour;

  if (timeNow <= 12) {
    return 'Good Morning';
  } else if ((timeNow > 12) && (timeNow <= 16)) {
    return 'Good Afternoon';
  } else if ((timeNow > 16) && (timeNow < 20)) {
    return 'Good Evening';
  } else {
    return 'Good Night';
  }
}

Widget hourlyBoxes(Forecast _forecast) {
  return Container(
      // margin: EdgeInsets.symmetric(vertical: 0.0),
      padding: EdgeInsets.only(left: 5, right: 5),
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding:
                const EdgeInsets.only(left: 18, top: 0, bottom: 0, right: 8),
            child: Text(
              "Hourly Forecast",
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                  color: Colors.black),
            ),
          ),
          Container(
            height: 150.0,
            child: ListView.builder(
                padding:
                    const EdgeInsets.only(left: 8, top: 0, bottom: 0, right: 8),
                scrollDirection: Axis.horizontal,
                itemCount: _forecast.hourly.length,
                itemBuilder: (context, index) {
                  return Container(
                      padding: const EdgeInsets.only(
                          left: 10, top: 10, bottom: 10, right: 10),
                      margin: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(18)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.1),
                              spreadRadius: 2,
                              blurRadius: 2,
                              offset:
                                  Offset(0, 1), // changes position of shadow
                            )
                          ]),
                      child: Column(children: [
                        Text(
                          "${_forecast.hourly[index].temp}°",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 17,
                              color: Colors.black),
                        ),
                        getWeatherIcon(_forecast.hourly[index].icon),
                        Text(
                          "${getTimeFromTimestamp(_forecast.hourly[index].dt)}",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 12,
                              color: Colors.grey),
                        ),
                        Text(
                          "${getDateFromTimestamp(_forecast.hourly[index].dt)}",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 12,
                              color: Colors.grey[200]),
                        ),
                      ]));
                }),
          ),
        ],
      ));
}

Widget threeHourlyBoxes(ForecastFiveDays _forecast) {
  return Expanded(
    child: Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding:
              const EdgeInsets.only(left: 23, top: 12, bottom: 0, right: 8),
          child: Text(
            "Daily Forecast",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontWeight: FontWeight.w500, fontSize: 12, color: Colors.black),
          ),
        ),
        Container(
          child: ListView.builder(
              padding:
                  const EdgeInsets.only(left: 8, top: 0, bottom: 0, right: 0),
              // scrollDirection: Axis.vertical,
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: _forecast.threehourly.length,
              // itemCount: 1,
              itemBuilder: (context, index1) {
                return Container(
                  padding: const EdgeInsets.only(
                      left: 10, top: 0, bottom: 0, right: 0),
                  margin: const EdgeInsets.all(5),
                  child: Stack(
                    children: [
                      Positioned(
                        top: 35,
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "${getDayDateFromTimestamp(_forecast.threehourly[index1][0].dt)}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                color: Colors.grey),
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(
                            left: 40, top: 0, bottom: 0, right: 0),
                        height: 90,
                        child: ListView.builder(
                            padding: const EdgeInsets.only(
                                left: 0, top: 0, bottom: 0, right: 0),
                            scrollDirection: Axis.horizontal,
                            itemCount: _forecast.threehourly[index1].length,
                            itemBuilder: (context, index2) {
                              return Container(
                                  alignment: Alignment.center,
                                  width: 70,
                                  padding: const EdgeInsets.only(
                                      left: 5, top: 5, bottom: 5, right: 5),
                                  margin: const EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(18)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 2,
                                          blurRadius: 2,
                                          offset: Offset(0,
                                              1), // changes position of shadow
                                        )
                                      ]),
                                  child: Column(children: [
                                    Text(
                                      "${_forecast.threehourly[index1][index2].temp}°",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 12,
                                          color: Colors.black),
                                    ),
                                    getWeatherIconSmall(_forecast
                                        .threehourly[index1][index2].icon),
                                    Text(
                                      "${getTimeFromTimestamp(_forecast.threehourly[index1][index2].dt)}",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 12,
                                          color: Colors.grey),
                                    ),
                                  ]));
                            }),
                      ),
                    ],
                  ),
                );
              }),
        ),
      ],
    ),
  );
}

class Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(0, size.height - 20);

    path.quadraticBezierTo((size.width / 6) * 1, (size.height / 2) + 15,
        (size.width / 3) * 1, size.height - 30);
    path.quadraticBezierTo((size.width / 2) * 1, (size.height + 0),
        (size.width / 3) * 2, (size.height / 4) * 3);
    path.quadraticBezierTo((size.width / 6) * 5, (size.height / 2) - 20,
        size.width, size.height - 60);

    path.lineTo(size.width, size.height - 60);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(Clipper oldClipper) => false;
}
